# How Not To F#

Projects, Notes, and other Resources Concerning a YouTube Series on F#

## About Me

Watch me code in F# live on twitch.tv at [https://www.twitch.tv/thegrumpygamedev](https://www.twitch.tv/thegrumpygamedev)

The stuff I do on stream is not exactly "new to F#" friendly.  Videos have been requested from viewers showing the basics of F#.

For the most part, information better than what I provide is freely available at Scott Wlaschin's website: [https://fsharpforfunandprofit.com/](https://fsharpforfunandprofit.com/)

As for me, I'm not an F# zealot, and do not believe that the entire world should code in F#. 

(Eventually, you may meet an F# person who **DOES** think this way. I'm just preparing you.)

## Target Audience (About You)

Lots of languages pay the bills, and when asked which one I suggest people learn, I tell them .... JavaScript.

But you like Python, Java, C#, C++, C, Rust or whatever - do as you will. Learn what you want. I just prefer F#.

My assumption for this series is that the viewer is modestly conversant with C#, and is interested in F# because it is also a .NET language, and C# has over the last years become gradually more functional.

## Subject Matter

I'm starting with the basics: primitives, operators and functions.

If I continue, I will gradually build up to more advanced topics.

I'm going to tell you what I think about things.

I'm going to show things that are possible, and many of them are non-idiomatic and are not the way one would write F# code.

Hence the title... How Not To F#.

By giving counter examples, maybe together we can rule out what not to do and instead do what we ought to.






