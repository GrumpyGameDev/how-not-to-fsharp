﻿using System;
using System.Linq;

namespace CSharpFunctionBasics
{
    class Program
    {
        static int OneParameterWithResult(int parameter)
        {
            return parameter + 1;
        }

        static int TwoParametersWithResult(int first, int second)
        {
            return first + second;
        }

        static int NoParameterWithResult()
        {
            return 10;
        }

        static void OneParameterNoResult(int parameter)
        {
            Console.WriteLine(parameter.ToString());
        }

        static void TwoParametersNoResult(int first, int second)
        {
            Console.WriteLine(first.ToString() + " " + second.ToString());
        }

        static void NoParametersNoResult()
        {
            Console.WriteLine("Hello!");
        }

        static Func<int, int> TwoCurriedParametersWithResult(int first)
        {
            Func<int, int> result = (x) => x + first;
            return result;
        }

        static void Main(string[] args)
        {
            int first = 1;
            int second = 2;

            int result = OneParameterWithResult(first);
            Console.WriteLine($"1. {result}");

            result = TwoParametersWithResult(first, second);
            Console.WriteLine($"2. {result}");

            result = NoParameterWithResult();
            Console.WriteLine($"3. {result}");

            Console.Write("4. ");
            OneParameterNoResult(first);

            Console.Write("5. ");
            TwoParametersNoResult(first,second);

            result = TwoCurriedParametersWithResult(first)(second);
            Console.WriteLine($"6. {result}");

            var partialApplication = TwoCurriedParametersWithResult(first);
            result = partialApplication(second);
            Console.WriteLine($"7. {result}");

            Console.Write("8. ");
            NoParametersNoResult();
        }
    }
}
