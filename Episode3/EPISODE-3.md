# Episode 3 - Function Basics

In this episode, I talk about the basics of functions in F#, from the perspective of a sojourner from C#.

Naturally, in a "functional first" language like F#, functions are a big topic and we cannot cover it in a single ten minute video.

As such, the goal of this video is to talk about what you, a presumed C# developer already know about functions, and how to make the equivalent functions in F#, keeping in mind that when you do so, you will be using F# "with a thick C# accent" and you will not be writing idomatic F# code.

But baby steps, right?

At the end, I will go over briefly the default way that F# likes to do functions with a curried function and partial application of it.  I made a C# equivalent, which will appear somewhat awkward in the imperative language.

However, I am just giving the briefest touch on curried functions, because their use, reason for being, and power are beyond this introductory scope.

## Show Notes

[My Twitch Stream](https://www.twitch.tv/thegrumpygamedev)

[TBD Gamer's Twitch Stream](https://www.twitch.tv/tbdgamer)

[Source Code For Episode](https://gitlab.com/GrumpyGameDev/how-not-to-fsharp/-/tree/master/Episode3)

[More about F# Functions](https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/functions/)