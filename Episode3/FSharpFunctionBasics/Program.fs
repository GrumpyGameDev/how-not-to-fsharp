﻿open System

let OneParameterWithResult(parameter:int) : int =
    parameter + 1

let TwoParametersWithResult(first:int, second:int) : int =
    first + second

let NoParameterWithResult() : int =
    10

let OneParameterNoResult(parameter:int) : unit =
    Console.WriteLine(parameter.ToString())

let TwoParametersNoResult(first:int, second:int) : unit =
    Console.WriteLine(first.ToString() + " " + second.ToString())

let NoParametersNoResult() : unit =
    Console.WriteLine("Hello!")
//                                     <=        =>
let TwoCurriedParametersWithResult (first:int) (second:int) : int =
    first + second

[<EntryPoint>]
let main _ =
    let first = 1
    let second = 2

    let result = OneParameterWithResult(first)
    printfn "1. %d" result

    let result = TwoParametersWithResult(first, second)
    printfn "2. %d" result

    let result = NoParameterWithResult()
    printfn "3. %d" result

    printf "4. "
    OneParameterNoResult(first)

    printf "5. "
    TwoParametersNoResult(first,second)

    let result = TwoCurriedParametersWithResult first second
    printfn "6. %d" result

    let partialApplication = TwoCurriedParametersWithResult first
    let result = partialApplication second
    printfn "7. %d" result

    let multiply first second = first * second
    let add first second = first + second

    let multiply5 = multiply 5
    let add8 = add 8
    let operation = multiply5 >> add8

    let result = 5 |> operation

    printf "8. "
    NoParametersNoResult()

    0
