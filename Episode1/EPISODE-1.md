# Episode 1 - Primitives
>F# isn't just typesafe... its typesaf-ER!

In this episode, I explore the commonly used primitive types from C# and how they may be used in F#.

This is not an exhaustive list of primitive types, and I especially and deliberately leave out the number of ways that strings and byte arrays are handled within F#.

I have a C# example, exhibiting the common integer, floating point, and string types, and I have an F# example showing the same things.

None of these differences are "good" or "bad"... they just _are_.

This is a series on how a person who knows C#, a language that has a lot of "slop" to it, can comprehend and adjust to some of the very "opinionated" ways that F# does things.

I hope you find this useful.

## Show Notes

[My Twitch Stream](https://www.twitch.tv/thegrumpygamedev)

[Source Code For Episode](https://gitlab.com/GrumpyGameDev/how-not-to-fsharp/-/tree/master/Episode1)

[More about F# Literals](https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/literals)
