﻿open System

[<EntryPoint>]
let main argv =
    let anSByte : sbyte   = 0y
    let aByte   : byte    = 0uy

    let aShort  : int16   = 0s
    let aUShort : uint16  = 0us

    let anInt   : int     = 0l  //the l is optional, thankfully
    let aUInt   : uint    = 0ul //the l is optional, thankfully
    let anInt   : int     = 0
    let aUInt   : uint    = 0u

    let aLong   : int64   = 0L
    let aULong  : uint64  = 0UL


    let aFloat  : float32 = 0.0f
    let aDouble : float   = 0.0

    //value promotion? not in F#!
    let signedTotal = (int64 anSByte) + (int64 aShort) + (int64 anInt) + aLong
    let unsignedTotal = (uint64 aByte) + (uint64 aUShort) + (uint64 aUInt) + aULong
    let realTotal = (float aFloat) + aDouble

    let aChar   : char    = '\000'
    let aString : string  = "" //...and strings get their own session, because they need it

    let concatenated = aString + aChar.ToString()

    0
