﻿namespace CSharpPrimatives
{
    class Program
    {
        static void Main(string[] args)
        {
            sbyte anSByte = 0;
            byte aByte = 0;

            short aShort = 0;
            ushort aUShort = 0;

            int anInt = 0;
            uint aUInt = 0;

            long aLong = 0;
            ulong aULong = 0;

            float aFloat = 0;
            double aDouble = 0;

            char aChar = '\0';
            string aString = "";

            long signedTotal = anSByte + aShort + anInt + aLong;
            ulong unsignedTotal = aULong + aByte + aUShort + aUInt;//aULong has to be first, but otherwise it works
            var realTotal = aFloat + aDouble;
            var concatenated = aString + aChar;

        }
    }
}
