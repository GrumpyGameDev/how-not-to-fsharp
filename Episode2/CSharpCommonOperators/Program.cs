﻿using System;

namespace CSharpCommonOperators
{
    class Program
    {
        static void Main(string[] args)
        {
            int first = 2;
            int second = 2;

            int sum = first + second;
            int difference = first - second;
            int product = first * second;
            int quotient = first / second;
            int modulus = first % second;

            bool isEqual = first == second;
            bool isNotEqual = first != second;
            bool isLessThan = first < second;
            bool isGreaterThan = first > second;

        }
    }
}
