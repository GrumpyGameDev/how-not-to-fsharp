﻿[<EntryPoint>]
let main _ =
    let first  : int = 2
    let second : int = 2

    //the "normal" way
    let sum        : int   = first +  second
    let difference : int   = first -  second
    let product    : int   = first *  second
    let quotient   : int   = first /  second
    let modulus    : int   = first %  second
    let exponent   : float = 2.0   ** 2.0      //doesn't work with ints

    let isEqual       : bool = first =  second
    let isNotEqual    : bool = first <> second
    let isLessThan    : bool = first <  second
    let isGreaterThan : bool = first >  second

    //operators as "functions"
    let sum        = (+) first second
    let difference = (-) first second
    let product    = (*) first second
    let quotient   = (/) first second
    let modulus    = (%) first second
    let exponent   = ( **) 2.0 2.0             //because (**) is a comment!

    let isEqual       = (=)  first second
    let isNotEqual    = (<>) first second
    let isLessThan    = (<)  first second
    let isGreaterThan = (>)  first second

    0
