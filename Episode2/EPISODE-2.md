# Episode 2 - Common Operators

In this episode, I talk about the common operators in F#, from the perspective of a sojourner from C#.

By "common" I do not mean the operators that are commonly used in F#, but rather the operators that a C# developer will be usd to.

So today, I'm not covering the pipe operator |> or the assignment operator <- or the cast operator :?>.

Really, I think forward pipe practically gets its own session, because it is that important.

So, we're going to stick to the common arithmetic operators - plus and minus and such- and the relational operators - equals and less than and so on.

## Show Notes

[My Twitch Stream](https://www.twitch.tv/thegrumpygamedev)

[Source Code For Episode](https://gitlab.com/GrumpyGameDev/how-not-to-fsharp/-/tree/master/Episode2)

[More about F# Operators](https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/symbol-and-operator-reference/)