# Episode 4 - Pipes and Other Plumbing

In this episode, I talk about the operators that truly stand out to newcomers to the language, most notably the forward pipe (|>), but also the backward pipe (<|) and the composition operator (>>).

These operators are the foundation of function and monadic (yeah, I said the word!) operations.

In many ways, these sorts of operations can be likened to fluent interfaces in C#, but as we shall see in the example, it is not a perfect comparison.

The forward pipe takes tuples from the left and curries them on the right. The backward pipe takes tuples from the right and curries them (also on the right). It enables a sort of "leap frogging" and monadic way of processing things.

The composition operator allows us to staple operations together so that it looks like a single operation.

## Show Notes

[My Twitch Stream](https://www.twitch.tv/thegrumpygamedev)

[TBD Gamer's Twitch Stream](https://www.twitch.tv/tbdgamer)

[Source Code For Episode](https://gitlab.com/GrumpyGameDev/how-not-to-fsharp/-/tree/master/Episode4)

[More about F# Pipes](https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/functions/#function-composition-and-pipelining)

[developers//stream](http://developers.stream/)