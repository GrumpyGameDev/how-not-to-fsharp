using System;

namespace CSharpPipesAndOtherPlumbing
{
    public class FluentCalculator
    {
        public int Value { get; private set; }
        public FluentCalculator(int value)
        {
            Value = value;
        }
        public FluentCalculator Add(int amount)
        {
            return new FluentCalculator(amount + Value);
        }
        public FluentCalculator Multiply(int amount)
        {
            return new FluentCalculator(amount * Value);
        }
        public FluentCalculator AddThenMultiply(int addAmount, int multiplyAmount)
        {
            return Add(addAmount).Multiply(multiplyAmount);
        }
        public FluentCalculator MultiplyThenAdd(int multiplyAmount, int addAmount)
        {
            return Multiply(multiplyAmount).Add(addAmount);
        }
    }
}
