﻿using System;

namespace CSharpPipesAndOtherPlumbing
{
    class Program
    {
        static void Main(string[] args)
        {
           int first = 2;
           int second = 3;
           int third = 5;

           int result = new FluentCalculator(first).Add(second).Value;
           Console.WriteLine($"1. {result}");

           result = new FluentCalculator(first).Multiply(second).Value;
           Console.WriteLine($"2. {result}");

           result = new FluentCalculator(first).Add(second).Multiply(third).Value;
           Console.WriteLine($"3. {result}");

           result = new FluentCalculator(first).AddThenMultiply(second, third).Value;
           Console.WriteLine($"4. {result}");

           result = new FluentCalculator(first).Multiply(second).Add(third).Value;
           Console.WriteLine($"5. {result}");

           result = new FluentCalculator(first).MultiplyThenAdd(second, third).Value;
           Console.WriteLine($"6. {result}");
        }
    }
}
