﻿open System

let add (first:int) (second:int) : int = first + second

let multiply (first:int) (second:int) : int = first * second

let addThenMultiplyPiped (addAmount:int) (multiplyAmount:int) (originalAmount:int): int =
    originalAmount
    |> add addAmount 
    |> multiply multiplyAmount

let addThenMultiply (addAmount:int) (multiplyAmount:int) : int -> int =

    add addAmount 
    >> multiply multiplyAmount

let multiplyThenAdd (multiplyAmount:int) (addAmount:int) : int -> int =
    multiply multiplyAmount 
    >> add addAmount

[<EntryPoint>]
let main _ =
    let first = 2
    let second = 3
    let third = 5

    let result = add first second
    printfn "1. %d" result

    let result = multiply first second
    printfn "2. %d" result

    let result = addThenMultiplyPiped first second third
    printfn "3. %d" result

    let result = addThenMultiply first second third
    printfn "4. %d" result

    let result = multiplyThenAdd first second third
    printfn "5. %d" result

    let result = third |> multiplyThenAdd first second
    printfn "6. %d" result

    let result = (second, third) ||> multiplyThenAdd first
    printfn "7. %d" result

    let result = (first, second, third) |||> multiplyThenAdd
    printfn "8. %d" result

    let result = multiplyThenAdd first second <| third
    printfn "9. %d" result

    let result = multiplyThenAdd first <|| (second, third)
    printfn "10. %d" result

    let result = multiplyThenAdd <||| (first, second, third)
    printfn "11. %d" result

    0
