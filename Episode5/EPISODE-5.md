# Episode  - The Tuples Strike Back

In this episode, I talk about the tuples. Tuples are not unique to F#, and may be found in newer revisions of C# operating in much the same way that they do in F#.

Tuples become rather important in a language like F# where you can only have one return type and you find yourself needed to return more than two values of differing types.

If you needed to return two values of a given type, you'd use a List, Array, or Sequence, which we'll get to in a future episode, but if you needed staple two unrelated things together, like an int and a string, you need a tuple.

Also in this episode, I pull in the F# core into C#, so we can have a look at it from the outside.

## Show Notes

[My Twitch Stream](https://www.twitch.tv/thegrumpygamedev)

[TBD Gamer's Twitch Stream](https://www.twitch.tv/tbdgamer)

[Source Code For Episode](https://gitlab.com/GrumpyGameDev/how-not-to-fsharp/-/tree/master/Episode4)

[More about F# Pipes](https://docs.microsoft.com/en-us/dotnet/fsharp/language-reference/functions/#function-composition-and-pipelining)

[developers//stream](http://developers.stream/)