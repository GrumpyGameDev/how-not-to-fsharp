﻿open System

type TwoInts = int * int
type ThreeInts = int * int * int

let twoInts : TwoInts = (1,2)
let threeInts : ThreeInts = (1,2,3)
let fourInts = (1,2,3,4)
let fiveInts = (1,2,3,4,5)
let sixInts = (1,2,3,4,5,6)
let sevenInts = (1,2,3,4,5,6,7)
let eightInts = (1,2,3,4,5,6,7,8)
let nineInts = (1,2,3,4,5,6,7,8,9)
let tenInts = (1,2,3,4,5,6,7,8,9,10)
let elevenInts = (1,2,3,4,5,6,7,8,9,10,11)
let twelveInts = (1,2,3,4,5,6,7,8,9,10,11,12)

[<EntryPoint>]
let main _ =
    printfn "1. %A" twoInts
    printfn "2. %A" threeInts
    printfn "3. %A" fourInts
    printfn "4. %A" fiveInts
    printfn "5. %A" sixInts
    printfn "6. %A" sevenInts
    printfn "7. %A" eightInts
    printfn "8. %A" nineInts
    printfn "9. %A" tenInts
    printfn "10. %A" elevenInts
    printfn "11. %A" twelveInts
    printfn "12. %A" <| twelveInts.GetType()

    let twoAndThree = (twoInts,threeInts)
    printfn "13. %A" twoAndThree
    let andFour = (twoAndThree, fourInts)
    printfn "14. %A" andFour
    let andFive = (andFour, fiveInts)
    printfn "15. %A" andFive
    let andSixAndSeven = (andFive, sixInts, sevenInts);
    printfn "16. %A" andSixAndSeven
    printfn "17. %A" <| andSixAndSeven.GetType()

    let twoUnits = ((),())
    printfn "18. %A" twoUnits
    let threeUnits = ((),(),())
    printfn "19. %A" threeUnits
    let twoAndThreeUnits = (twoUnits, threeUnits)
    printfn "20. %A" twoAndThreeUnits
    printfn "21. %A" <| twoAndThreeUnits.GetType()

    let first = twoAndThreeUnits |> fst
    printfn "22. %A" first
    let second = twoAndThreeUnits |> snd
    printfn "23. %A" second

    //alternate extraction method
    let first, second = twoAndThreeUnits
    printfn "22a. %A" first
    printfn "23a. %A" second

    0
