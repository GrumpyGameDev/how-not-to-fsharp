﻿using Microsoft.FSharp.Core;
using System;

namespace CSharpTheTuplesStrikeBack
{
    class Program
    {
        static (int, int) twoInts = (1,2);
        static (int,int,int) threeInts = (1,2,3);
        static (int,int,int,int) fourInts = (1,2,3,4);
        static (int,int,int,int,int) fiveInts = (1,2,3,4,5);
        static (int,int,int,int,int,int) sixInts = (1,2,3,4,5,6);
        static (int,int,int,int,int,int,int) sevenInts = (1,2,3,4,5,6,7);
        static (int,int,int,int,int,int,int,int) eightInts = (1,2,3,4,5,6,7,8);
        static (int,int,int,int,int,int,int,int,int) nineInts = (1,2,3,4,5,6,7,8,9);
        static (int,int,int,int,int,int,int,int,int,int) tenInts = (1,2,3,4,5,6,7,8,9,10);
        static (int,int,int,int,int,int,int,int,int,int,int) elevenInts = (1,2,3,4,5,6,7,8,9,10,11);
        static (int,int,int,int,int,int,int,int,int,int,int,int) twelveInts = (1,2,3,4,5,6,7,8,9,10,11,12);

        static void Main(string[] args)
        {
            Console.WriteLine($"1. {twoInts}");
            Console.WriteLine($"2. {threeInts}");
            Console.WriteLine($"3. {fourInts}");
            Console.WriteLine($"4. {fiveInts}");
            Console.WriteLine($"5. {sixInts}");
            Console.WriteLine($"6. {sevenInts}");
            Console.WriteLine($"7. {eightInts}");
            Console.WriteLine($"8. {nineInts}");
            Console.WriteLine($"9. {tenInts}");
            Console.WriteLine($"10. {elevenInts}");
            Console.WriteLine($"11. {twelveInts}");
            Console.WriteLine($"12. {twelveInts.GetType()}");

            var twoAndThree = (twoInts,threeInts);
            Console.WriteLine($"13. {twoAndThree}");
            var andFour = (twoAndThree, fourInts);
            Console.WriteLine($"14. {andFour}");
            var andFive = (andFour, fiveInts);
            Console.WriteLine($"15. {andFive}");
            var andSixAndSeven = (andFive, sixInts, sevenInts);
            Console.WriteLine($"16. {andSixAndSeven}");
            Console.WriteLine($"17. {andSixAndSeven.GetType()}");

            var twoUnits = ((Unit)null, (Unit)null);
            Console.WriteLine($"18. {twoUnits}");
            var threeUnits = ((Unit)null, (Unit)null, (Unit)null);
            Console.WriteLine($"19. {threeUnits}");
            var twoAndThreeUnits = (twoUnits, threeUnits);
            Console.WriteLine($"20. {twoAndThreeUnits}");
            Console.WriteLine($"21. {twoAndThreeUnits.GetType()}");

            var first = Operators.Fst(twoAndThreeUnits.ToTuple());
            Console.WriteLine($"22. {first}");
            var second = Operators.Snd(twoAndThreeUnits.ToTuple());
            Console.WriteLine($"23. {second}");

            var (f, s) = twoAndThreeUnits;
            Console.WriteLine($"22a. {f}");
            Console.WriteLine($"23a. {s}");
        }
    }
}
